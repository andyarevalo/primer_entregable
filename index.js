﻿function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
    
  var clave = txtClave.value;
  var valor = txtValor.value;
  
   sessionStorage.setItem(clave, valor);
  
     
  var objeto = {
    nombre:"Andy",
    apellidos:"Arevalo Bracamonte",
    ciudad:"Lima",
    pais:"Perú"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));
}

    
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
 spanValor.innerText = valor;
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function removerEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.removeItem(clave);  
} 

  function limpiarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.clear();  
  } 
  
  function lengthDeSessionStorage() {
   elementos = sessionStorage.length;
   spanValor.innerText = elementos;
  }
 
 